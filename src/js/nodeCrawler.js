/*
Main Node.js program that will crawl the url received.

Main inpout parameters:
	mainUrl (string) = url received to be crawled
	maxPagesCrawled (int)= Max number of pages to be crawled, we need this to reach an out condition.
	maxDepth (int)= depth to explore under each page.

It uses an existing Node.js libary 'js-crawler' which does the crawling and returns an object of type page with some basic info
for every single page crawled.

Funtion: crawlSite:
This function uses the previous   output to populates a Map(url, node Object), where url is the key and node Object has an object of PageCrawled type
with urlParnet, url, title and nodes {array of children urs}. As the program crawls every single page is stored in the map and 
if the parentUrl already exist then the url is added to the array of nodes to the corresponding parent.

When the program reaches maxPagesCrawled or the end of the crawling if less than that, it generates the JSON out put.

Funtion: crawledJSON:
Goes through out the Map(url, object) generating recursivelly the JSON output, so for each element in the map it generates the header json
and then for each element of the node array it goes to that address in the map and recursively generates the json output.
Finally it returns the JSON representation of the crwled site.

*/

var Crawler = require("js-crawler");
var cheerio = require("cheerio")
var map = require("collections/map")
var PageCrawled = require("./PageCrawled.js")


//var mainUrl = "http://www.sydneyflooring.com.au/";
//var map = new Map();
var normalExit = false;
var maxDepth = 7;


//This is in to generate a JSON output in case the program is killed 
process.on('beforeExit', (code) => {
	console.log("beforeExit beforeExit")
	if (normalExit ){
		normalExit = false;
		var finalJSON = crawledJSON();
		console.log('\n FINAL FINAL JSON:' + finalJSON);

		return (finalJSON);

	}
});


/*
process.on('SIGINT', () => {
  console.log('Received SIGINT.  Press Control-D to exit.');
});
*/

/*
This function will generate the JSON representation needed by going to the map, chosing the first item and calling the recursive fucntion
*/
function crawledJSON(rootUrl, map){

	console.log('\nGenerating final JSON...');

	var crawJSON = helperCrawledJSON(  map.get(rootUrl) , map);
	//to avoid runnig twice this due to asyncronism
	normalExit=true;

	return ('[' + crawJSON) + ']';

}


//Recursive function that returns the crawled places in JSON format
// The exit cindition is when there are no nodes to proces for each page.
// Else, It creates the header o fthe JSON for each node and then for each member of the body it  recursively gets the JSON
// finally it returns
function helperCrawledJSON( pageCrawled, map){

	//first exit condition of recursive function
	if (!pageCrawled){
		return  ( JSON.stringify( pageCrawled));
	}else if (pageCrawled.nodes.length == 0 ){ //Second exit condition of recursive function, all linked pages where exausted
		return  ( JSON.stringify( pageCrawled));
	} else{ //otherwise keep on going
		var headJSON = '{"parentUrl":' + JSON.stringify(pageCrawled.parentUrl) + ','
		  			+ '"url":' + JSON.stringify(pageCrawled.url) + ','
		  			+ '"title":' + JSON.stringify(pageCrawled.title) + ','
		  			+ '"nodes":['
		 
		 //Building JSON for each internal node
		 var crawledTreeJSON='';
		 for (var i = 0;  i < pageCrawled.nodes.length ; i++){
		 	var urlToExplore = pageCrawled.nodes[i];

		 	crawledTreeJSON +=  helperCrawledJSON(  map.get( urlToExplore )) ;

		 	//Adds a coma between each memeber of the node's list if needed.
		 	if (i < pageCrawled.nodes.length -1 ){
		 		crawledTreeJSON += ',';
		 	} 
		}
		//returning JSON representation of  a node
		return( headJSON +  crawledTreeJSON + '] }');
	}
}



/*
//Main crawler function. 
*/

exports.crawlSite = function(req, res) {
	//console.log('Crawling with parameters: ' + JSON.stringify(req.body) );

	var mainUrl = req.body.url;
	var maxPagesCrawled = req.body.maxPagesToCrawl;
	maxDepth = req.body.maxDepth;

	var map = new Map();
	var pagesCrawled = 0;
	normalExit=false;

	crawler.forgetCrawled();

	console.log('\n\n ################# \n');
	console.log('Crawling with parameters: ');
	console.log('mainUrl: ' + mainUrl);
	console.log('maxPagesCrawled: ' + maxPagesCrawled);
	console.log('maxDepth: ' + maxDepth);
	
	res.setHeader('Content-Type', 'application/json');
	//res.setHeader('charset','utf-8');

	crawlSite(mainUrl , maxPagesCrawled, pagesCrawled, map, function(resultJSON){

		pagesCrawled=0;

		res.end(resultJSON);
		//res.json(resultJSON);
	});

	
}


var crawler = new Crawler().configure({
	depth: maxDepth,
	maxRequestsPerSecond: 10,
    maxConcurrentRequests: 5
});


/*
//helper crawler function. For every page it crawls it returns a page objects with all the info from the page
*/
function crawlSite(mainUrl, maxPagesCrawled, pagesCrawled, map, cb){

	//crawler.forgetCrawled();
	//map.clear();

	crawler.crawl({
		url: mainUrl,

		success: function (page) {

			var self = this

			//check exit condition by reaching max number of pages to be crawled
			if (pagesCrawled === maxPagesCrawled ){
				console.log("Max number of pages crawled reached . Finishing..." + pagesCrawled + ", " + maxPagesCrawled);
				self._currentUrlsToCrawl = [];
				self._finishedCrawling();
			}else {

			    var $ = cheerio.load(page.content);
			    var parentUrl = page.referer
			    var webPageTitle = $("title").text();
		        
		        var pageCrawled = new PageCrawled(page);

		        console.log("\nADDING: " );
		        console.log("map.length: " + map.length);
		        console.log("Parent: " + page.referer);
			    console.log("url: " + page.url + "\n");


		        // if parentUrl is null then this is the root, add it once
			    if (parentUrl===""){
			    	pageCrawled.parentUrl = mainUrl;
			    	map.add(pageCrawled, mainUrl)

			    }else if (!map.has( pageCrawled.parentUrl ) ){
			    	
			    	map.add(pageCrawled, page.url);

			    }else{ // page already exist, then add page visited

			    	// getting object from map to add url
			    	pageCrawledTemp=map.get(pageCrawled.parentUrl);
			    	
			    	pageCrawledTemp.addChildPage(page.url);
			    	
			    	map.set(pageCrawled.parentUrl, pageCrawledTemp);

			    	//adding one more node to the map
			    	// the framework won't repeat same page
			    	map.add(pageCrawled, pageCrawled.url)

			    }

			    pagesCrawled++;
			}
		 },

		 failure: function(page){
		 	console.log('\nfailure...');
		  	console.log(page.status);
		 },

		 finished: function(crawledUrls){
		 	console.log('finished');
		 	var finalJSON = crawledJSON(mainUrl, map);
			cb (finalJSON);
		 }
	});

}


