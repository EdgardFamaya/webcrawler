var gulp = require('gulp');
var gulpif = require('gulp-if');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var util = require('gulp-util');
var newer = require('gulp-newer');

// folders
  folder = {
    src: './src/',
    build: './build/'
  }


gulp.task('clean', function () {
    return gulp.src([
        './build/js/*.js'
    ], {read: false})
        .pipe(clean());
});

gulp.task('default', function () {
    var uglifyFlag = util.env.envName === 'production';

    return gulp.src(folder.src + 'js/*.js')
        .pipe(newer(folder.build + 'js/'))
        .pipe(gulpif(uglifyFlag, uglify({mangle: true})))
        .pipe(gulpif(uglifyFlag, sourcemaps.write()))
        .pipe(gulp.dest(folder.build   + 'js/'));
});