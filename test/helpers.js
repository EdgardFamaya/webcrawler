var supertest = require('supertest');  
var chai = require('chai');  
var uuid = require('uuid');  
var app = require('../build/js/indexCrawler.js');
var should = require('should'); 

global.app = app;  
global.uuid = uuid;  
global.expect = chai.expect;  
global.should = should;
global.request = supertest(app);  
