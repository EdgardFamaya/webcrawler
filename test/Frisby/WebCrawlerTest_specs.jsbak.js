var frisby = require('frisby');


it('Should get header content type json'), function(doneFn){
  frisby.get('http://localhost:8083/crawlSite',{
      "url" : "http://www.sydneyflooring.com.au/",
      "maxPagesToCrawl" : 40,
      "maxDepth" : 10
  }, {json: true})
  .expectHeaderContains('content-type', 'json')
  .done(doneFn);
});