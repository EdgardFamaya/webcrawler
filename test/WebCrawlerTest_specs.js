



 
describe('returns an small treet ', function() {
    this.timeout(30000);


    it('Expecting returned Obj equal to expected for one page only', function(done) {
    	this.retries(2);
    	var siteTreet = JSON.parse('[{"parentUrl":"http://www.sydneyflooring.com.au/","url":"http://www.sydneyflooring.com.au/","title":"Amazing Timber Flooring in Sydney","nodes":[]}]');
        
        request.get('/crawlSite')
	  	    .send({
	      		url : "http://www.sydneyflooring.com.au/",
	    		maxPagesToCrawl : 1,
	     		maxDepth : 2
	  			})
	  	    .set('Content-Type', 'application/json')
	  	    .set('Accept','application/json')
	  	    .expect(200)
	  	    .end (function (err, res){
	  	    	if (err){
	  	    		throw err;
	  	    	}
	  	    	var resJSONObjReceived = JSON.parse(res.text);
	  	    	
	  	    	//res.text.should.have.property('parentUrl');
	  	    	
	  	    	expect(resJSONObjReceived).to.eql(siteTreet);
	  	    	
	  	    	done(err);
	  	    });
    });



	it('Expecting the output to be an object', function(done) {
        request.get('/crawlSite')
	  	    .send({
	      		url : "http://www.sydneyflooring.com.au/",
	    		maxPagesToCrawl : 1,
	     		maxDepth : 1
	  			})
	  	    .set('Content-Type', 'application/json')
	  	    .set('Accept','application/json')
            .expect(200)
            .end(function(err, res) {
        		console.log('XXXXXXXXX res: ' + JSON.stringify(res));
        		var resJSONObjReceived2 = JSON.parse(res.text);
        		console.log('resJSONObjReceived: '+ JSON.stringify(resJSONObjReceived2));
        		var parentUrl = JSON.stringify(resJSONObjReceived2.parentUrl);
        		console.log('parentUrl: ' + parentUrl);
        		expect(parentUrl).to.eql('"http://www.sydneyflooring.com.au/"');
                done(err);
            });
    });


  it('Should get a 200 header.', function(doneFn){
	  	request.get('/crawlSite')
	  	    .send({
	      		url : "http://www.sydneyflooring.com.au/",
	    		maxPagesToCrawl : 3,
	     		maxDepth : 3
	  			})
			.set('Content-Type', 'application/json')
			.end(function(err, res){
				
				expect(res.status).to.eql(200);
				doneFn(err);
			});
	});	


  
});	


