/*
REST Routes for GET , POST and others if needed.
*/

'use strict';

module.exports = function(app) {
	var nodeCrawler = require('./nodeCrawler');

	app.route('/crawlSite')
		.get( nodeCrawler.crawlSite );
	
};