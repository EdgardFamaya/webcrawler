/*
Main HTTP Server that will listen for REST connections to the api and will enroute requirements to the crawlerRoutes defined
The HTTP server will run in localhost:8083
a variable process.env.PORT can be set up with a different port
*/
const express = require('express')
const bodyParser = require('body-parser')
const port = process.env.PORT || 8083
const app = express()

// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json 
app.use(bodyParser.json());


var routes = require('./crawlerRoutes');
routes(app);


var server = app.listen(port, function(){
	var host = server.address().address;
	var port = server.address().port;
	console.log("HTTP Server listening at http://%s:%s",host,port);
});

module.exports = server;

