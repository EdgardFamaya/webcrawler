
/*
Basic class that stores some basic info returned by the crawler
	parenturl : Parent of the page
	url: url of the page
	title: Title of the page
	nodes[]: Is an array of children  urls where the parent is the url field of this object
*/
var cheerio = require("cheerio")


function PageCrawled() {
	this.parentUrl = '';
	this.url = '';
	this.title = '';
	this.nodes=[];
};


function PageCrawled(page) {
	var $ = cheerio.load(page.content);
	this.parentUrl = page.referer;
	this.url = page.url;
	this.title = $("title").text();
	this.nodes=[];

	
};
PageCrawled.prototype.addChildPage = function (childPage) {
	this.nodes.push(childPage);
};

module.exports = PageCrawled;