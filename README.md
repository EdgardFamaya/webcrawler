# README #

This is a simple Node.js REST  api web crawler. It receives an URL as a parameter and create a tree of child pages linked to the URL.
It also expect the max number of pages to crawl and the depth of the crawling. Finally it generates an output of JSON type.

![JSONTreeImage.png](images/JSONTreeImage.png)

I had designed and build four Node.js components:

indexCrawler.js --> crawlerRoutes.js --> nodeCrawler.js -> {uses} -> PageCrawled.js


##PageCrawled.js: ##
Basic class that stores some basic info returned by the crawler
	parenturl : Parent of the page
	url: url of the page
	title: Title of the page
	nodes[]: Is an array of children urls where the parent is the url field of this object


## indexCrawler.js ##
Main HTTP Server that will listen for REST connections to the api and will enroute requirements to the crawlerRoutes defined
The HTTP server will run in localhost:8083
a variable process.env.PORT can be set up with a different port


##crawlerRoutes.js: ##
REST Routes for GET , POST and others if needed.


## nodeCrawler.js: ##
Main Node.js program that will crawl the url received.

Input parameters:

	mainUrl (string) = url received to be crawled.

	maxPagesCrawled (int)= Max number of pages to be crawled, we need this to reach an out condition.

	maxDepth (int)= depth to explore under each page.

It uses an existing Node.js libary 'js-crawler' which does the crawling and returns an object of type page with some basic info for every single page crawled.


Funtion: crawlSite:

This function uses the previous   output to populates a Map(url, node Object), where url is the key and node Object has an object of PageCrawled type
with urlParent, url, title and nodes {array of children urs}. As the program crawls every single page is stored in the map and 
if the parentUrl already exist then the url is added to the array of nodes to the corresponding parent.

When the program reaches maxPagesCrawled or the end of the crawling if less than that, it generates the JSON output.

Map created is like this

![mapImage.png](images/mapImage.png)



Funtion: crawledJSON:

Goes through out the Map(url, object) generating recursivelly the JSON output, so for each element in the map it generates the header json
and then for each element of the node array it goes to that address in the map and recursively generates the json output.
Finally it returns the JSON representation of the crwled site.



### How do I get set up? ###

* Summary of set up

You need:

Maven (mvn) latest, java JDK 7 or grether of course for maven.


* Configuration

Clone this repository or copy a zip of the project.

cd your_project_folder

mvn clean package {This one will download all dependencies found in the package.json (node, testing, etc)}


* Executing

cd your_project_folder/build/js

node indexCrawler.js 

{if sucess it should show a message: HTTP Server listening at http://:::8083}


* Dependencies

You don't need to individually download these dependencies as the maven project does it for you, I just want to mention them:

js-crawler {node.js library to crawl sites}

gulp {compiles your Node.js api and move it to ../build/js. used by mvn}

mocha, chai, supertest {javascrip modules for testing}

express {http server}


## How to run tests ##

* If you want to run the set of test writen with mocha, chai and supertest please do:

First kill or Ctrl-c the instance if running.

from your_project_folder

npm test


* If you want to individully test the api rest service you can use cli:



cd your_project_folder/build/js

node indexCrawler.js 

curl -i -H "Content-Type: application/json" -H "Accept: application/json" -XGET "http://localhost:8083/crawlSite" -d'
{
    
        "url" : "http://www.smh.com.au/",
        "maxPagesToCrawl" : 18,
        "maxDepth" : 5
    
}'

Note:

The Get /crawlSite  receives a json objects as parameter


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: 

Edgard Amaya famaya@yahoo.com

* Other community or team contact